package android.avito.com.avitotest;

import android.app.Application;
import android.avito.com.avitotest.injection.CommonModule;

import roboguice.RoboGuice;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:07
 */
public class AvitoTestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initInjections();
    }

    private void initInjections() {
        RoboGuice.overrideApplicationInjector(this,
                RoboGuice.newDefaultRoboModule(this), new CommonModule());
    }

}
