package android.avito.com.avitotest.backend;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 17:26
 * Простой класс для отправки хттп запросов, реализован только HTTP метод GET, этого пока достаточно
 * todo Реализовать POST, PUT, DELETE
 */
public class HttpClient {
    private final static int CONNECTION_TIMEOUT = 30*1000; // 30 sec timeout

    /**
     * GET запрос
     * @param url
     * @return
     */
    protected String get(String url) {
        try {
            return read(sendRequest(new HttpGet(url)));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private InputStream sendRequest(HttpRequestBase httpRequest) throws IOException {
        org.apache.http.client.HttpClient httpclient   = new DefaultHttpClient();
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpResponse response   = httpclient.execute(httpRequest);
        HttpEntity entity       = response.getEntity();

        return entity.getContent();
    }

    private static String read(InputStream instream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(instream));
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        instream.close();

        return sb.toString();
    }
}
