package android.avito.com.avitotest.backend.request;

import android.avito.com.avitotest.backend.Github;

import com.octo.android.robospice.request.SpiceRequest;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:16
 * Базовый класс для запросов на апи github'a, подставляет Github клиент в метод loadDataFromNetwork
 */
abstract public class GithubRequest<T> extends SpiceRequest<T> {
    private Github github;

    public GithubRequest(Class<T> clazz) {
        super(clazz);
    }

    public void bindClient(Github github) {
        this.github = github;
    }

    @Override
    final public T loadDataFromNetwork() throws Exception {
        return loadDataFromNetwork(github);
    }

    abstract public T loadDataFromNetwork(Github github) throws Exception;
}
