package android.avito.com.avitotest.injection.provider;

import android.avito.com.avitotest.service.GithubServiceHelper;

import com.google.inject.Provider;
import com.octo.android.robospice.SpiceManager;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:52
 */
public class SpiceManagerProvider implements Provider<SpiceManager> {
    @Override
    public SpiceManager get() {
        return new GithubServiceHelper();
    }
}
