package android.avito.com.avitotest.ui;

import android.avito.com.avitotest.R;
import android.avito.com.avitotest.backend.entity.GithubRepository;
import android.avito.com.avitotest.backend.entity.GithubUser;
import android.avito.com.avitotest.backend.entity.SearchResult;
import android.avito.com.avitotest.util.ViewHolder;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:27
 */
public class SearchResultAdapter extends RecyclerView.Adapter<ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<SearchResult> searchResultList;

    final static int TYPE_USER = 1000;
    final static int TYPE_REPOSITORY = 2000;

    public SearchResultAdapter(Context context, List<SearchResult> searchResults) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.searchResultList = searchResults;
    }

    @Override
    public int getItemViewType(int position) {
        SearchResult result = searchResultList.get(position);
        if(result instanceof GithubUser) {
            return TYPE_USER;
        } else {
            return TYPE_REPOSITORY;
        }
    }

    private ViewHolder createUserViewHolder(ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_user, parent, false);
        return new ViewHolder(view, R.id.user_login, R.id.user_avatar);
    }

    private ViewHolder createRepositoryViewHolder(ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_repository, parent, false);
        return new ViewHolder(view, R.id.repo_name, R.id.repo_description, R.id.repo_forks);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case TYPE_USER:
                return createUserViewHolder(viewGroup);
            case TYPE_REPOSITORY:
                return createRepositoryViewHolder(viewGroup);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        SearchResult searchResult = searchResultList.get(i);
        if(searchResult instanceof GithubUser) {
            GithubUser githubUser = (GithubUser)searchResult;
            viewHolder.setText(R.id.user_login, githubUser.getLogin());

            Picasso.with(context)
                    .load(githubUser.getAvatarUrl())
                    .into((ImageView)viewHolder.get(R.id.user_avatar));
        } else {
            GithubRepository githubRepository = (GithubRepository)searchResult;
            viewHolder.setText(R.id.repo_name, githubRepository.getName());
            viewHolder.setText(R.id.repo_description, githubRepository.getDescription());
            viewHolder.setText(R.id.repo_forks, Integer.toString(githubRepository.getForksCount()));
        }
    }

    @Override
    public int getItemCount() {
        return searchResultList.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
