package android.avito.com.avitotest.ui;

import android.avito.com.avitotest.R;
import android.avito.com.avitotest.backend.RequestFactory;
import android.avito.com.avitotest.backend.entity.SearchResult;
import android.avito.com.avitotest.backend.entity.SearchResultList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


@ContentView(R.layout.activity_github_search)
public class GithubSearchActivity extends RoboActionBarActivity implements SearchView.OnQueryTextListener, View.OnClickListener {
    private final static String KEY_SEARCH_RESULT = "key_state";

    @Inject
    private SpiceManager spiceManager;
    @Inject
    private RequestFactory requestFactory;

    private ArrayList<SearchResult> resultList;

    /**
     * Сохраненное состояние активити, используется после пересоздания активити
     */
    private SearchActivityState restoredActivityState;

    @InjectView(android.R.id.list)
    private RecyclerView recyclerView;

    @InjectView(R.id.progress)
    private ProgressBar progressBar;

    @InjectView(R.id.start_search)
    private Button searchButton;

    @InjectView(R.id.empty)
    private TextView empty;

    private MenuItem searchItem;
    private SearchView searchView;

    private static final int MESSAGE_TEXT_CHANGED = 100;

    /**
     * Задержка перед отправкой запроса, после ввода текста в строке поиска
     */
    private static final int REQUEST_DELAY = 1500; // 1,5 секунды

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            performSearchRequest((String) msg.obj);
        }
    };

    /**
     * Класс обработчик ответа от сервиса
     */
    RequestListener<SearchResultList> requestListener = new RequestListener<SearchResultList>() {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(GithubSearchActivity.this, getString(R.string.load_data_error), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(SearchResultList githubUsers) {
            resultList = githubUsers;
            setListAdapter(new SearchResultAdapter(GithubSearchActivity.this, githubUsers));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchButton.setOnClickListener(this);

        // если сохраненные данные получаем их
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey(KEY_SEARCH_RESULT)) {
                restoredActivityState = savedInstanceState.getParcelable(KEY_SEARCH_RESULT);
                searchButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_github_users, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                hideAllViews();
                return true;
            }
        });
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                resetActivityState();
                return true;
            }
        });

        if(restoredActivityState != null) {
            searchView = restoreSearchView(searchItem, restoredActivityState);
        } else {
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        }

        searchView.setQueryHint(getString(R.string.search));
        searchView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    /**
     * Восстанавливает состояние SearchView
     * @param searchMenuItem
     * @param searchActivityState
     * @return
     */
    private SearchView restoreSearchView(MenuItem searchMenuItem, SearchActivityState searchActivityState) {
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        if(searchActivityState.isExpanded()) {
            MenuItemCompat.expandActionView(searchMenuItem);
        } else {
            MenuItemCompat.collapseActionView(searchMenuItem);
        }

        if(!TextUtils.isEmpty(searchActivityState.getQuery())) {
            searchView.setQuery(searchActivityState.getQuery(), false);
        }

        return searchView;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // сохраняем состояние активити
        outState.putParcelable(KEY_SEARCH_RESULT, new SearchActivityState(searchView.getQuery().toString(),
                        MenuItemCompat.isActionViewExpanded(searchItem), resultList));

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_search:
                view.setVisibility(View.GONE);
                MenuItemCompat.expandActionView(searchItem);
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // Отписываемся от всех слушателей
        spiceManager.dontNotifyAnyRequestListeners();
        resultList = null;
        if(newText.length() > 1) {
            showProgress();
            // удаляем предыдущее сообщение
            mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
            // отправляем сообщение с задержкой, на случай если пользователь вводит подряд несколько символов
            mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, newText),
                    REQUEST_DELAY);
        } else {
            hideAllViews();
        }

        return false;
    }

    /**
     * Сбрасывает состояние активити до начального
     */
    private void resetActivityState() {
        hideAllViews();
        resultList = null;
        searchButton.setVisibility(View.VISIBLE);
    }

    /**
     * Скрывает все элементы view
     */
    private void hideAllViews() {
        empty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        searchButton.setVisibility(View.GONE);
    }

    /**
     * Показывает прогресс
     */
    private void showProgress() {
        hideAllViews();
        progressBar.setVisibility(View.VISIBLE);
    }

    public void setListAdapter(RecyclerView.Adapter adapter) {
        progressBar.setVisibility(View.GONE);
        if(adapter.getItemCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }

        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
        if(restoredActivityState != null) {
            if(restoredActivityState.getResults() != null) {
                resultList = restoredActivityState.getResults();
                setListAdapter(new SearchResultAdapter(this, resultList));
            } else if(!TextUtils.isEmpty(restoredActivityState.getQuery())) {
                showProgress();
                performSearchRequest(restoredActivityState.getQuery());
            }
        }
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    /**
     * Выполняет запрос на поиск
     */
    private void performSearchRequest(String query) {
        spiceManager.execute(requestFactory.createSearchRequest(query), requestListener);
    }
}
