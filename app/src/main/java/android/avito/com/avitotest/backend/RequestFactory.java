package android.avito.com.avitotest.backend;

import android.avito.com.avitotest.backend.entity.GithubUserList;
import android.avito.com.avitotest.backend.entity.SearchResultList;

import com.octo.android.robospice.request.SpiceRequest;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:39
 * Интерфейс вспомогательного класса - создателя запросов, реализация должна в себе инкапсулировать создание запроса
 * и всегда возвращать базовый класс SpiceRequest<?>
 */
public interface RequestFactory {
    public SpiceRequest<SearchResultList> createSearchRequest(String q);
    public SpiceRequest<GithubUserList> createGetUsersRequest();
}
