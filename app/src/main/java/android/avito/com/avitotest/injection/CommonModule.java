package android.avito.com.avitotest.injection;

import android.avito.com.avitotest.backend.Github;
import android.avito.com.avitotest.backend.RequestFactory;
import android.avito.com.avitotest.injection.provider.GithubClientProvider;
import android.avito.com.avitotest.injection.provider.RequestFactoryProvider;
import android.avito.com.avitotest.injection.provider.SpiceManagerProvider;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.octo.android.robospice.SpiceManager;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:49
 */
public class CommonModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(Github.class).toProvider(GithubClientProvider.class).in(Singleton.class);
        binder.bind(RequestFactory.class).toProvider(RequestFactoryProvider.class).in(Singleton.class);
        binder.bind(SpiceManager.class).toProvider(SpiceManagerProvider.class);
    }
}