package android.avito.com.avitotest.backend;

import android.avito.com.avitotest.backend.entity.GithubRepository;
import android.avito.com.avitotest.backend.entity.GithubRepositoryList;
import android.avito.com.avitotest.backend.entity.GithubUser;
import android.avito.com.avitotest.backend.entity.GithubUserList;
import android.util.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 17:32
 * Реализация Github клиента
 */
public class GithubImpl extends HttpClient implements Github {
    private final static String HOST = "https://api.github.com";

    private String buildUrl(String method) {
        return HOST + method;
    }

    private GithubUserList readSearchUserList(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            return readUserList(jsonObject.getString("items"));
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private GithubUserList readUserList(String response) {
        GithubUserList users = new GithubUserList();
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length(); i++) {
                users.add(readUser(jsonArray.getString(i)));
            }

            return users;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private GithubUser readUser(String response) {
        GithubUser user = new GithubUser();
        try {
            JSONObject jsonObject = new JSONObject(response);
            user.setLogin(jsonObject.getString("login"));
            user.setAvatarUrl(jsonObject.getString("avatar_url"));

            return user;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private GithubRepositoryList readSearchRepositoryList(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            return readRepositoryList(jsonObject.getString("items"));
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private GithubRepositoryList readRepositoryList(String response) {
        GithubRepositoryList repositories = new GithubRepositoryList();
        try {
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length(); i++) {
                repositories.add(readRepository(jsonArray.getString(i)));
            }

            return repositories;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private GithubRepository readRepository(String response) {
        GithubRepository repository = new GithubRepository();
        try {
            JSONObject jsonObject = new JSONObject(response);
            repository.setName(jsonObject.getString("name"));
            repository.setDescription(jsonObject.getString("description"));
            repository.setForksCount(jsonObject.getInt("forks_count"));

            return repository;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public GithubUserList getUsers() {
        return readUserList(get(buildUrl("/users")));
    }

    @Override
    public GithubRepositoryList searchRepositories(String q) {
        return readSearchRepositoryList(get(buildUrl("/search/repositories?q=" + q)));
    }

    @Override
    public GithubUserList searchUsers(String q) {
        return readSearchUserList(get(buildUrl("/search/users?q=" + q)));
    }
}
