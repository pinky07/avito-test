package android.avito.com.avitotest.injection.provider;

import android.avito.com.avitotest.backend.RequestFactory;
import android.avito.com.avitotest.backend.RequestFactoryImpl;

import com.google.inject.Provider;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:51
 */
public class RequestFactoryProvider implements Provider<RequestFactory> {
    @Override
    public RequestFactory get() {
        return new RequestFactoryImpl();
    }
}
