package android.avito.com.avitotest.ui;

import android.avito.com.avitotest.backend.entity.SearchResult;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Author: Khamidullin Kamil
 * Date: 10.08.15
 * Time: 18:10
 */
public class SearchActivityState implements Parcelable {
    private String query;
    private ArrayList<SearchResult> results;
    private boolean isExpanded;

    public SearchActivityState(String query, boolean isExpanded, ArrayList<SearchResult> results) {
        this.query = query;
        this.isExpanded = isExpanded;
        this.results = results;
    }

    public String getQuery() {
        return query;
    }

    public ArrayList<SearchResult> getResults() {
        return results;
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.query);
        dest.writeSerializable(this.results);
        dest.writeByte(isExpanded ? (byte) 1 : (byte) 0);
    }

    private SearchActivityState(Parcel in) {
        this.query = in.readString();
        this.results = (ArrayList<SearchResult>) in.readSerializable();
        this.isExpanded = in.readByte() != 0;
    }

    public static final Creator<SearchActivityState> CREATOR = new Creator<SearchActivityState>() {
        public SearchActivityState createFromParcel(Parcel source) {
            return new SearchActivityState(source);
        }

        public SearchActivityState[] newArray(int size) {
            return new SearchActivityState[size];
        }
    };
}
