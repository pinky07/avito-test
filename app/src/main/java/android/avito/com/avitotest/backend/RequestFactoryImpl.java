package android.avito.com.avitotest.backend;

import android.avito.com.avitotest.backend.entity.GithubUserList;
import android.avito.com.avitotest.backend.entity.SearchResultList;
import android.avito.com.avitotest.backend.request.SearchRequest;
import android.avito.com.avitotest.backend.request.UsersRequest;

import com.octo.android.robospice.request.SpiceRequest;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:54
 */
public class RequestFactoryImpl implements RequestFactory {
    @Override
    public SpiceRequest<SearchResultList> createSearchRequest(String q) {
        return new SearchRequest(q);
    }

    @Override
    public SpiceRequest<GithubUserList> createGetUsersRequest() {
        return new UsersRequest();
    }
}
