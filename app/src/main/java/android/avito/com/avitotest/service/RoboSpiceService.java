package android.avito.com.avitotest.service;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;

import com.google.inject.Injector;
import com.google.inject.Key;
import com.octo.android.robospice.SpiceService;

import java.util.HashMap;
import java.util.Map;

import roboguice.RoboGuice;
import roboguice.context.event.OnConfigurationChangedEvent;
import roboguice.context.event.OnCreateEvent;
import roboguice.context.event.OnDestroyEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.util.RoboContext;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:13
 */
abstract public class RoboSpiceService extends SpiceService implements RoboContext {
    protected EventManager eventManager;
    protected HashMap<Key<?>,Object> scopedObjects = new HashMap<Key<?>, Object>();

    @Override
    public void onCreate() {
        /*robo*/
        final Injector injector = RoboGuice.getInjector(this);
        eventManager = injector.getInstance(EventManager.class);
        injector.injectMembers(this);
        super.onCreate();
        eventManager.fire(new OnCreateEvent<Service>(this, null));
        /******/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int startCont = super.onStartCommand(intent,flags, startId);
        eventManager.fire(new OnStartEvent<Service>(this));
        return startCont;
    }

    @Override
    public void onDestroy() {
        try {
            if(eventManager!=null)
                eventManager.fire(new OnDestroyEvent<Service>(this) );
        } finally {
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                super.onDestroy();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        eventManager.fire(new OnConfigurationChangedEvent<Service>(this,currentConfig, newConfig) );
    }

    @Override
    public Map<Key<?>, Object> getScopedObjectMap() {
        return scopedObjects;
    }
}
