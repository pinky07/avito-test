package android.avito.com.avitotest.service;

import android.app.Application;
import android.app.Service;
import android.avito.com.avitotest.backend.Github;
import android.avito.com.avitotest.backend.request.GithubRequest;

import com.google.inject.Inject;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.binary.InFileBitmapObjectPersister;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.string.InFileStringObjectPersister;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Set;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:09
 * Реализация SpiceService, подставляет реализацию Github в класс - запрос наследующий GithubRequest
 */
public class GithubService extends RoboSpiceService {
    @Inject
    Github github;

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new InFileStringObjectPersister(application));
        cacheManager.addPersister(new InFileBitmapObjectPersister(application));
        return cacheManager;
    }

    @Override
    public void addRequest(CachedSpiceRequest<?> request, Set<RequestListener<?>> listRequestListener) {
        if (request.getSpiceRequest() instanceof GithubRequest<?>) {
            ((GithubRequest<?>)request.getSpiceRequest()).bindClient(github);
        }
        super.addRequest(request, listRequestListener);
    }
}
