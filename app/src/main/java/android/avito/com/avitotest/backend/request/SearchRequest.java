package android.avito.com.avitotest.backend.request;

import android.avito.com.avitotest.backend.Github;
import android.avito.com.avitotest.backend.entity.GithubRepositoryList;
import android.avito.com.avitotest.backend.entity.GithubUserList;
import android.avito.com.avitotest.backend.entity.SearchResultList;

/**
 * Author: Khamidullin Kamil
 * Date: 10.08.15
 * Time: 13:21
 */
public class SearchRequest extends GithubRequest<SearchResultList> {
    private String query;

    public SearchRequest(String query) {
        super(SearchResultList.class);
        this.query = query;
    }

    @Override
    public SearchResultList loadDataFromNetwork(Github github) throws Exception {
        SearchResultList searchResultList = new SearchResultList();
        GithubUserList githubUsers = github.searchUsers(query);
        GithubRepositoryList githubRepositories = github.searchRepositories(query);

        int totalResultCount = githubUsers.size() + githubRepositories.size();

        int reposCounter = 0;
        int usersCounter = 0;
        for(int i = 1; i <= totalResultCount; i++) {
            if((i % 2 == 0 && reposCounter < githubRepositories.size()) || usersCounter >= githubUsers.size()) {
                searchResultList.add(githubRepositories.get(reposCounter));
                reposCounter ++;
            } else {
                searchResultList.add(githubUsers.get(usersCounter));
                usersCounter ++;
            }
        }

        return searchResultList;
    }
}
