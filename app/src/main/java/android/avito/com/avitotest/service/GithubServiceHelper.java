package android.avito.com.avitotest.service;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.SpiceService;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:53
 */
public class GithubServiceHelper extends SpiceManager {

    public GithubServiceHelper() {
        super(GithubService.class);
    }
}
