package android.avito.com.avitotest.backend.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 17:22
 */
public class GithubUser implements Parcelable, SearchResult {
    private String login;
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeString(this.avatarUrl);
    }

    public GithubUser() {
    }

    private GithubUser(Parcel in) {
        this.login = in.readString();
        this.avatarUrl = in.readString();
    }

    public static final Creator<GithubUser> CREATOR = new Creator<GithubUser>() {
        public GithubUser createFromParcel(Parcel source) {
            return new GithubUser(source);
        }

        public GithubUser[] newArray(int size) {
            return new GithubUser[size];
        }
    };
}
