package android.avito.com.avitotest.util;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

/**
 * Author: Khamidullin Kamil
 * Date: 10.08.15
 * Time: 15:27
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> holder = new SparseArray<View>();

    public ViewHolder(View itemView, int... ids) {
        super(itemView);
        putAllViewsFromParent(itemView, ids);
    }

    @SuppressWarnings("unchecked")
    public <T extends View> T get(int id) {
        return (T) holder.get(id);
    }

    public TextView textView(int id) {
        return (TextView) holder.get(id);
    }

    public TextView setText(int id, String text) {
        TextView textView = textView(id);
        textView.setText(text);

        return textView;
    }

    public void setVisibleText(int id, String text) {
        if(TextUtils.isEmpty(text)) {
            textView(id).setVisibility(View.GONE);
        } else {
            setText(id, text).setVisibility(View.VISIBLE);
        }
    }

    public void put(View view, int id) {
        holder.put(id, view);
    }

    public void put(View view) {
        put(view, view.getId());
    }

    public void putViewFromParent(View parent, int id) {
        View view = parent.findViewById(id);
        put(view, id);
    }

    private void putAllViewsFromParent(View itemView, int... ids) {
        for(int id: ids) {
            putViewFromParent(itemView, id);
        }
    }

    public void clear() {
        holder.clear();
    }
}
