package android.avito.com.avitotest.backend.request;

import android.avito.com.avitotest.backend.Github;
import android.avito.com.avitotest.backend.entity.GithubUser;
import android.avito.com.avitotest.backend.entity.GithubUserList;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:21
 * Класс - запрос на получение списка пользователей
 */
public class UsersRequest extends GithubRequest<GithubUserList> {
    public UsersRequest() {
        super(GithubUserList.class);
    }

    @Override
    public GithubUserList loadDataFromNetwork(Github github) throws Exception {
        return github.getUsers();
    }
}
