package android.avito.com.avitotest.backend;

import android.avito.com.avitotest.backend.entity.GithubRepositoryList;
import android.avito.com.avitotest.backend.entity.GithubUserList;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 17:24
 */
public interface Github {
    /**
     * Получение списка пользователей
     * @return
     */
    public GithubUserList getUsers();

    public GithubRepositoryList searchRepositories(String q);

    public GithubUserList searchUsers(String q);
}
