package android.avito.com.avitotest.backend.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 10.08.15
 * Time: 13:21
 */
public class GithubRepository implements Parcelable, SearchResult {
    private String name;
    private String description;
    private int forksCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.forksCount);
    }

    public GithubRepository() {
    }

    private GithubRepository(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.forksCount = in.readInt();
    }

    public static final Creator<GithubRepository> CREATOR = new Creator<GithubRepository>() {
        public GithubRepository createFromParcel(Parcel source) {
            return new GithubRepository(source);
        }

        public GithubRepository[] newArray(int size) {
            return new GithubRepository[size];
        }
    };
}
