package android.avito.com.avitotest.injection.provider;

import android.avito.com.avitotest.backend.Github;
import android.avito.com.avitotest.backend.GithubImpl;

import com.google.inject.Provider;

/**
 * Author: Khamidullin Kamil
 * Date: 27.07.15
 * Time: 18:50
 */
public class GithubClientProvider implements Provider<Github> {
    @Override
    public Github get() {
        return new GithubImpl();
    }
}
