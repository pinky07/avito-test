package android.avito.com.avitotest.backend.entity;

import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 10.08.15
 * Time: 13:22
 */
public interface SearchResult extends Parcelable {
}
